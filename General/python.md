## Python Coding Standards:

1. Indentation and Formatting:
   • Use 4 spaces for each level of indentation.
   • Limit all lines to a maximum of 79 characters for code and 72 for docstrings.
   • Follow PEP 8 guidelines for code formatting.
2. Imports:
   • Import standard libraries first, followed by third-party libraries, and finally, your own modules.
   • Avoid wildcard imports.
   • Don’t use import \*, import what’s required
3. Whitespace:
   • Use blank lines to separate functions, classes, and blocks of code inside functions.
   • Avoid trailing whitespace at the end of lines.
4. Naming Conventions:
   • Use descriptive names for variables, functions, and classes.
   • Follow PEP 8 conventions for naming.
5. Documentation:
   • Provide docstrings for all modules, classes, and functions.
   • Use comments sparingly and make them meaningful.
6. Error Handling:
   • Handle exceptions gracefully and provide informative error messages.
   • Avoid using bare except clauses.
7. Testing:
   • Write unit tests for your code using a testing framework like unittest or pytest.
   • Ensure that your tests cover different scenarios.
8. Others:
   • Always use f stings instead of concatenating variables with plus ‘+’ sign.
   • Use with statement to open/auto close any file.
   • Use context manager instead of try finally exception handling (where resources need to be closed)
   • Never use empty except clause. E.g “except Exception as e”
   • Default parameters (Mutable) in a function parameter should be initialized as None.
   • Learn and use comprehensions (List, dictionary, set, generators).
   • Use Liskov substitution principle. E.g. Isinstance() instead of type().
   • While looping over dict, use copy of keys or values not actual.
   • Measure functions time complexity using perf*counter.
   • Use logging instead of print.
   • Don’t define path in a string instead use pathlib library.
   • Separate i/o function and its functionality.
   • Avoid using global variables.
   • Do not delete or insert while iterating, create a separate list or set to store value then perform those operations
   • Use * to define unused variable (in loops or functions), '\_variable' to declare private/class/function variables, '\_\_variable' to declare protected variables
   • Follow SOLID principle, (Single module single responsibility, Open/Closed functionality if function works do not add more code complexity to its (Use Liskov principle), Interface segregation and dependency inversion - Use SQRS and Mediator patterns )
   • Use dataclass(wrapper function)
