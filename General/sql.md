## SQL (MySQL, PostgreSQL) Coding Standards:

1. Naming Conventions:
   • Use descriptive and consistent names for tables, columns, and indexes.
   • Avoid using reserved words like SELECT, UNION etc.

2. Indentation and Formatting:
   • Indent SQL queries for better readability.
   • Use uppercase for SQL keywords.

3. Parameterized Queries:
   • Use parameterized queries to prevent SQL injection.
   • Avoid concatenating user inputs directly into queries.

4. Indexes and Optimization:
   • Index columns used in WHERE clauses for performance.
   • Regularly review and optimize complex queries.
5. Transactions:
   • Use transactions for operations that require atomicity.
   • Handle errors and rollbacks appropriately.
