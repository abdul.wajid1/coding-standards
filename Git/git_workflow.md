# Git Workflow and Project Structure Guide

Welcome to the Git Workflow and Project Structure Guide! This document provides detailed instructions on how to effectively manage your codebase with Git, what to include and exclude, and best practices for maintaining a clean project structure.

## Table of Contents

1. Git Basics
2. What to Push to Git
3. What Not to Push to Git
4. Project Directory Structure
5. Python Project Structure
6. Best Practices
7. Getting Started
8. Additional Resources

## Git Basics

### Common Commands

- Clone a repository: git clone <repository_url>
- Check status: git status
- Stage changes: git add <file_or_directory>
- Commit changes: git commit -m "commit message"
- Push changes: git push origin <branch_name>
- Pull updates: git pull origin <branch_name>
- Create a branch: git checkout -b <branch_name>
- Switch branches: git checkout <branch_name>

### Branching Strategy

- Main branch: main or master (stable code ready for production)
- Development branch: develop (latest development changes)
- Feature branches: feature/<feature_name> (new features or improvements)
- Bugfix branches: bugfix/<bug_name> (bug fixes)
- Release branches: release/<version_number> (preparing for a new release)

### What to Push to Git

- Source code: All application code files.
- Configuration files: E.g., settings.py, webpack.config.js.
- Documentation: README files, API docs.
- Build scripts: E.g., build.sh, Dockerfile.
- Version control files: E.g., package.json, requirements.txt.

### What Not to Push to Git

- Sensitive information: API keys, passwords, secrets.
- Generated files: Compiled code, binary files, build directories.
- Log files: Application logs.
- Local environment settings: IDE configuration files.
- Large files: Large datasets, media files that can be hosted elsewhere.

### Example .gitignore for Python

```
# Byte-compiled / optimized / DLL files
__pycache__/
*.py[cod]
*$py.class

# Distribution / packaging
.Python
build/
develop-eggs/
dist/
downloads/
eggs/
.eggs/
lib/
lib64/
parts/
sdist/
var/
wheels/
*.egg-info/
.installed.cfg
*.egg

# PyInstaller
#  Usually these files are written by a python script from a template
#  before PyInstaller builds the exe, so as to inject date/other infos into it.
*.manifest
*.spec

# Installer logs
pip-log.txt
pip-delete-this-directory.txt

# Unit test / coverage reports
htmlcov/
.tox/
.nox/
.coverage
.coverage.*
.cache
nosetests.xml
coverage.xml
*.cover
.hypothesis/
.pytest_cache/
.pyre/

# Translations
*.mo
*.pot

# Django stuff:
*.log
*.pot
*.pyc
*.pyo
__pycache__/
local_settings.py
db.sqlite3
media

# Flask stuff:
instance/
.webassets-cache

# Scrapy stuff:
.scrapy

# Sphinx documentation
docs/_build/

# PyBuilder
target/

# Jupyter Notebook
.ipynb_checkpoints

# IPython
profile_default/
ipython_config.py

# Environments
.env
.venv
env/
venv/
ENV/
env.bak/
venv.bak/

# mypy
.mypy_cache/
.dmypy.json
dmypy.json
```

## Project Directory Structure

### General Structure

```
project-root/
│
├── src/                   # Source files
│   ├── main/              # Main application code
│   ├── tests/             # Unit tests
│   └── resources/         # Static resources
│
├── docs/                  # Documentation files
│
├── .gitignore             # Git ignore file
├── README.md              # Project README
├── requirements.txt       # Python dependencies
├── setup.py               # Setup script for Python packages
└── tox.ini                # Configuration for Tox
```

### Python Project Structure

```
project-root/
│
├── project_name/          # Project package
│   ├── __init__.py        # Package initializer
│   ├── module1.py         # Module 1
│   ├── module2.py         # Module 2
│   └── ...
│
├── tests/                 # Test suite
│   ├── __init__.py
│   ├── test_module1.py
│   └── test_module2.py
│
├── scripts/               # Utility scripts
│   └── data_processing.py
│
├── .gitignore             # Git ignore file
├── README.md              # Project README
├── requirements.txt       # Python dependencies
├── setup.py               # Setup script for Python packages
└── tox.ini                # Configuration for Tox
```

### Best Practices

- Keep commits small and focused: Make each commit a logical unit of change.
- Write meaningful commit messages: Describe what and why, not just how.
- Use branches: Develop features and fix bugs in separate branches.
- Code reviews: Always get your code reviewed by a peer.
- Regularly pull changes: Stay up to date with the main branch to avoid conflicts.
- Document your code: Maintain clear and updated documentation.

## Getting Started

1. Clone this repository: git clone <repository_url>
2. Navigate to the project directory: cd project_name
3. Create a new branch: git checkout -b feature/my-feature
4. Make your changes: Edit, add, and commit your code.
5. Push your branch to the remote repository: git push origin feature/my-feature
6. Create a Pull Request: Compare your branch with the main branch and create a Pull Request for review.

## Additional Resources

- [Git Documentation](https://git-scm.com/doc/)
- [Python Official Documentation](https://docs.python.org/3/)
- [Best Practices for Writing Python Code](https://gitlab.com/abdul.wajid1/coding-standards/-/tree/main/General?ref_type=heads)

Happy coding! If you have any questions, don't hesitate to ask for help or refer to the additional resources provided.
